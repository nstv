# Copyright (C) 2008 Robert Vally

# This file is part of nstv (Now Showing TV).

# nstv is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# nstv is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with nstv.  If not, see <http://www.gnu.org/licenses/>.

import re
import urllib
import string
import sys
import getopt
import datetime, time
from nstv_config import Config

# CONSTANTS
TVNZ_LISTINGS="""http://tvnz.co.nz/content/listings_data/tvnz_listings_all_skin"""
CHANNEL_NAME_PATTERN = "<td class=\"epg_logo_spacer\">.*/tvnz_epg_(.*)_logo_sm\.jpg"
CHANNEL_LISTING_PATTERN = "<table class=\"epg_border\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n<tr height=\"55\">\n((.*)\n)*?</tr>\n</table>"
PROGRAMME_LISTING_PATTERN = "title=\"(.+?)\".*\n<br>([0-9]*:[0-9]*)"
# The file in which the raw html will be placed once
# collected from the listing source (TVNZ_LISTINGS)
RAW_LISTING_FILE="""raw"""
#  END CONSTANTS

# collects all the listing data (HTML) from the specified source
# and returns the result
def fetch_listing(source):
	try:
		f = urllib.urlopen(source)
		data = f.read()
		f.close()
		return data;
	except:
		print 'Failed to fetch listing from source "%s"' % TVNZ_LISTINGS
		sys.exit()
  
# saves the supplied data (HTML) into the specified target location (local)
def save_listing(target, data):
	# Save raw data to the specified target
	try:
		print sys.path[0]
	  	s = open(sys.path[0] + '/' + target, 'w')
		s.write(data)
	  	s.close()
	except:
		print 'Failed to save listing to local cache file "%s"' % RAW_LISTING_FILE
		print 'Things will not work without it'
		sys.exit()

def load_listing(source):
	data = None
	try:
		path = ''
		if len(sys.path[0]) > 0:
			path = sys.path[0] + '/'
		f = open(path + source, 'r')
		data = f.read()
		f.close()
		return data
	except IOError:
		print 'Failed to open listing file "%s".' % source
		sys.exit()

# returns the channel list and all the progs
def parse_listing(raw):
	progs = []
	channels = []
  
	# pattern for matching each channel name
	p = re.compile(CHANNEL_NAME_PATTERN)
	channels = p.findall(raw)

	# pattern for matching each channel programmes
	p = re.compile(CHANNEL_LISTING_PATTERN)

	# pattern for matching time and programme description
	p2 = re.compile(PROGRAMME_LISTING_PATTERN)

	count = 0
	for m in p.finditer(raw):
		result = p2.findall(m.group(0))

		for r in result:
			progs.append([count, {'time': r[1], 'title': r[0]}])
      
		count = count + 1

	return channels, progs


def print_out(li, chan_filter, fmt):
	# $c\t- $t @ $st
	# ($c)\t- $t @ $st

	# check for a grouping in the format
	r = re.match(".*\((.*)\)(.*)", fmt)

	# if there is no grouping, just do a straight replace
	if r == None:
		for p in li:
			if chan_filter == None or p['channel'] in chan_filter:
				p_fmt = fmt
				p_fmt = p_fmt.replace("$c", p['channel'])
				p_fmt = p_fmt.replace("$t", p['title'])
				p_fmt = p_fmt.replace("$st", p['time'])
				print p_fmt
	# if there is a grouping
	else:
		group_by = r.group(1)
		group_by = group_by.replace("$c", "channel")
		group_by = group_by.replace("$t", "title")
		group_by = group_by.replace("$st", "time")
    
		group_by_prev = None
		for p in li:
			if chan_filter == None or p['channel'] in chan_filter:
				if group_by_prev == None or group_by_prev != p[group_by]:
					print p[group_by]
          
				p_fmt = r.group(2)
				p_fmt = p_fmt.replace("$c", p['channel'])
				p_fmt = p_fmt.replace("$t", p['title'])
				p_fmt = p_fmt.replace("$st", p['time'])
				print p_fmt

				group_by_prev = p[group_by]
     
def chan_filter(progs, chan):
	tmp = [elem for elem in progs if elem[0] == chan]
	return tmp

def prog_filter(progs, start_time, end_time):
	tmp = progs

	if start_time != None:
		# check if there is something starting at the start time
		match = [elem for elem in tmp 
				if datetime.time(int(elem[1]['time'].split(":")[0]), 
					int(elem[1]['time'].split(":")[1])) == start_time]

		if match:
			tmp = tmp[tmp.index(match[0]):]
		else:
			# find everything that falls before the start time
			before = [elem for elem in tmp 
					if datetime.time(int(elem[1]['time'].split(":")[0]), 
						int(elem[1]['time'].split(":")[1])) <= start_time]
			# pop off what is last in the list... this is what is on now
			if before:
				before = before.pop()
				tmp = tmp[tmp.index(before):]
			else:
				tmp = tmp[:]
	if end_time != None:
		tmp = [elem for elem in tmp
			if datetime.time(int(elem[1]['time'].split(":")[0]), 
				int(elem[1]['time'].split(":")[1])) < end_time]
	return tmp

# picks out which programmes are showing at the specified time across all channels
def on_at(channels, progs, cur_time, end_time, limit):
	li = []
	count = 0

	while count <= len(channels) - 1:
		limit_count = 0
		filtered = prog_filter(chan_filter(progs, count), cur_time, end_time)

		if (filtered and limit != None):
			filtered = filtered[:int(limit)]

		for cur_prog in filtered:
			li.append({'channel': channels[count],
						'title': cur_prog[1]['title'],
						'time': cur_prog[1]['time']})

		count = count + 1

	return li

# picks out programmes with specific titles
def on_when(channels, progs, title):
	# list which will contain all matched programmes
	li = []

	count = 0
	while count <= len(channels) - 1:
		tmp = [elem for elem in progs if elem[0] == count]
		for cur_prog in tmp:
			if title.upper() in cur_prog[1]['title'].upper():
				li.append({'channel':channels[count], 
					'title':cur_prog[1]['title'], 'time':cur_prog[1]['time']})
		count = count + 1

	return li
  
# picks out all programmes scheduled today
def on_today(channels, progs):
	# list which will contain all matched programmes
	li = []

	count = 0
	while count <= len(channels) - 1:
		tmp = [elem for elem in progs if elem[0] == count]
		for cur_prog in tmp:
			li.append({'channel':channels[count], 'title':cur_prog[1]['title'], 'time':cur_prog[1]['time']})
		count = count + 1

	return li

def usage():
	print 'NSTV - Now Showing TV'
	print 'A TV listing parser and reporting utility designed for use on the command line.'
	print '\nOptions:'
	print '  -f, --fetch        fetch (otherwise use from cached)'
	print '  -r, --raw          print raw HTML data (debugging)'
	print '\nFilter(s):'
	print '  -p, --programme    only show programmes with the specified name'
	print '  -n, --now [0-9]    what''s on now specifing the number ' + \
								'of programmes to show per channel'
	print '  -t, --today        what''s on today'
	print '  -a, --at [hh:mm]   filter out entries starting before'
	print '  -e, --ends [hh:mm] filter out entires starting after'
	print '  -c, --channel      comma delimited list of channels to filter by'
	print '  -h, --help         help'

def main(argv):
	# by default we will always worked from cached info
	fetch = False
	raw = False
	chan_filter = None
	output_type = {}
	end_time = None
		
	try:
		# get command line options
		opts, args = getopt.getopt(argv, "p:ha:e:n:tfc:o:", [
		"title",
		"help", 
		"at",
		"ends", 
		"now", 
		"today", 
		"fetch", 
		"channel",
		"override"])
	except getopt.GetoptError, err:
		# if problem with parsing command line
		# show usage and exit
		print 'ERROR: ' + str(err) + '\n'
		usage()
		sys.exit(2)

	# cycle through all the command line arguments
	# opt stores the actual command, and arg the values these
	# may or may not be assigned
	for opt, arg in opts:
		# print usage and shutdown
		if opt in ("-h", "--help"):
			usage()
			sys.exit()
		elif opt in ("-f", "--fetch"):
			fetch = True
		elif opt in ("-r", "--raw"):
			raw = True
		elif opt in ("-n", "--now"):
			output_type['n'] = arg
		elif opt in ("-t", "--today"):
			output_type['t'] = arg
		elif opt in ("-a", "--at"):
			output_type['a'] = arg
		elif opt in ("-e", "--ends"):
			output_type['e'] = arg
		elif opt in ("-c", "--channel"):
			output_type['c'] = arg
		elif opt in ("-o", "--override"):
			output_type['o'] = arg
		elif opt in ("-p", "--programme"):
			output_type['p'] = arg

	# fetch new listings if we've been told to
	if fetch:
		data = fetch_listing(TVNZ_LISTINGS)
		save_listing(RAW_LISTING_FILE, data)

	# load data from the cached file
	data = load_listing(RAW_LISTING_FILE)

	# parse data into channel and programme lists
	channels, progs = parse_listing(data)

	# set the channel filter
	if 'c' in output_type:
		chan_filter = output_type['c']
    
	if 'n' in output_type: #now
		cur_time = datetime.time(datetime.datetime.now().hour, 
			datetime.datetime.now().minute)
		end_time = None
		limit_num = 1

		if output_type['n'] != None:
			limit_num = output_type['n']
   
		# print out at least 1 showing programme from each channel
		print_out(on_at(channels, progs, cur_time, end_time, limit_num), 
			chan_filter, "$c\t- $t @ $st")
	elif 't' in output_type: #today
		cur_time = datetime.time(0, 0)
    
		end_time = None
		# if an end time has been specified, set it
		if 'e' in output_type:
			end_time = output_type['e'].split(":")
			end_time = datetime.time(int(end_time[0]), int(end_time[1]))
    
		print_out(on_at(channels, progs, cur_time, end_time, None), chan_filter, 
			"($c)\t- $t @ $st")
	elif 'a' in output_type: #at
		# set the start time
		if output_type['a'] != None:
			cur_time = output_type['a'].split(":")
		cur_time = datetime.time(int(cur_time[0]), int(cur_time[1]))
      
		end_time = None
		# if an end time has been specified, set it
		if 'e' in output_type:
			end_time = output_type['e'].split(":")
			end_time = datetime.time(int(end_time[0]), int(end_time[1]))
		else:
			# if no end time was given... add 1 minute to the start time
			end_time = datetime.time(cur_time.hour, cur_time.minute + 1)

		print_out(on_at(channels, progs, cur_time, end_time, None), chan_filter, 
			"($c)\t- $t @ $st")
	elif 'p' in output_type: #programme 
		if output_type['p'] != None:
			print_out(on_when(channels, progs, output_type['p']), chan_filter, "$c\t- $t @ $st")
  
	# useful for debugging
	if raw:
		print channels
		print progs

if __name__ == "__main__":
	main(sys.argv[1:])
  
