# Copyright (C) 2008 Robert Vally

# This file is part of nstv (Now Showing TV).

# nstv is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# nstv is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with nstv.  If not, see <http://www.gnu.org/licenses/>.

import ConfigParser

class Config:
	config_file = None

	def __init__(self):
		None

	def Config(filename='.nstvrc'):
		self.config_file = filename
		return self
